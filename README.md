# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Installation

We expect you to know how to setup Lumen via Composer. Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## The challenge

First of all thank you for participating in our backend code challenge.

### Headsup:

There is command you find called `ClientMatchesDaemonCommand`, which will run a daemon to fetch betting event data from one of our existing providers.

### Goal:

You are expected to fill out the client_matches  and client_matches_your_fake tables with the fetched data.

### Task breakdown:

More precisely you are supposed to:

1. Create a seeder of random data for tables 'matches', 'rosters' and 'match_opponents'
2. Create entity `ClientMatchesYourFake` and make `ClientMatchesYourFake` as JOINED inherence type (https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/inheritance-mapping.html) 
3. Create another "custom" fetcher which will use seeded matches from the database to fill out `client_matches` and `client_matches_your_fake` tables, with creating randomized odds analog to the data you can find in "SomeClient" Client Matches

### Stretch-goals related to this challenge are:

1. You cover your additionals by tests (use tdd).
2. Find three (3) hidden bugs
3. Do a proposal of possible improvements of test and readme file.

### Developer notes:
I added a docker environment with all configurations included.
To build the system you need to have pre-installed docker and docker-compose, then follow this:

1. `make build`
2. `make ssh`
3. `php artisan migrate --seed`
4. `php artisan client-matches:daemon` or `php artisan client-matches:import -s YourFakeCodename` 

And make sure in your .env file of this configs:

APP_URL=http://localhost
SOMECLIENT_ENDPOINT=http://localhost

This code challenger was amazing for me, I never worked with Doctrine but I found it amazing. The same with the code structure.

I really hope this code achieve minimum requirements, please send me your suggestions/questions,

I can expend more time for today, but I can continue doing enhancements if is needed.

Best of lucks,

Regards!  

PD: I found 2 bugs, there are in the last commit.
    About improvements, I added 2 Makefiles to automatize some tasks, 
    like testing and report generation. 
    I dind see any cache in the code, so it would be good to think about it, also add validations,
    send error via slack or send then to an error system.
    
    



