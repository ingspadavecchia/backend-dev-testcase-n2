<?php

namespace App\Fetchers;

use App\Fetchers\Entities\YourFakeEvents;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SomeClientEventsFetcher
 * @package App\Fetchers
 */
class YourFakeEventsFetcher extends AbstractFetcher
{
    /**
     * @param $mapper
     * @return mixed|void
     */
    public function fetch($mapper)
    {
        $eventsGroup   = [];
        $entityManager = app(EntityManagerInterface::class);
        // prepare data
        foreach (YourFakeEvents::ODD_TYPES as $oddType) {
            $response = $this->getResponse($this->buildURL($oddType));
            $events   = new YourFakeEvents($response, $oddType);
            $mapper->prepare($events);
            $eventsGroup[] = $events;
        }

        // map events to matches
        foreach ($eventsGroup as $events) {
            $mapper->map($events);
        }
        // save matches (execute query)
        $entityManager->flush();
    }

    /**
     * @param $oddType
     * @return string
     */
    protected function buildURL($oddType)
    {
        return $this->importConfig['parser']['endpoint']
            . $this->importConfig['parser']['path']
            . $this->importConfig['parser']['query']
            . '&OddsType=' . $oddType;
    }
}
