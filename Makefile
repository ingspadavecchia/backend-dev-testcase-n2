.PHONY: start build stop ssh

start:
	@echo "Starting docker images"
	docker-compose -f docker/docker-compose.yml up -d
build:
	@echo "Building docker images"
	docker-compose -f docker/docker-compose.yml rm
	docker-compose -f docker/docker-compose.yml pull
	docker-compose -f docker/docker-compose.yml build --no-cache
	docker-compose -f docker/docker-compose.yml up -d --force-recreate
stop:
	@echo "Stopping docker images"
	docker-compose -f docker/docker-compose.yml down
ssh:
	docker exec -it web /bin/sh