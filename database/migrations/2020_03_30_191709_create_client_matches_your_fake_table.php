<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientMatchesYourFakeTable extends Migration
{

    const CASCADE = 'cascade';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_matches_your_fake', function (Blueprint $table) {

            $table->increments('id');
            $table->text('odds');
            $table->text('new_odds');
            $table->text('odds_hk');
            $table->text('new_odds_hk');
            $table->text('odds_malay');
            $table->text('new_odds_malay');
            $table->text('odds_indo');
            $table->text('new_odds_indo');

            $table->string('special_name', 60)->nullable();

            $table->foreign('id')->references('id')->on('client_matches')
                  ->onUpdate(self::CASCADE)->onDelete(self::CASCADE);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_matches_your_fake');
    }
}
