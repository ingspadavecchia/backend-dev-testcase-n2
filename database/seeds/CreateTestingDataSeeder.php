<?php

use App\Entities\Match;
use App\Entities\Roster;
use Illuminate\Database\Seeder;

class CreateTestingDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i = 0; $i < 10; $i++) {

            $opponent1 = entity(Roster::class)->create();
            $opponent2 = entity(Roster::class)->create();

            $match = entity(Match::class)->create();

            $match->setOpponents($opponent1, $opponent2);

        }

    }
}
